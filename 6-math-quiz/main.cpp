#include <iostream>
#include <ucm_random>
using namespace std;

void displayQuestion(int number, int left, int right, int op) {
    char opSymbol;

    if (op == 1) {
        opSymbol = '+';
    }
    else if (op == 2) {
        opSymbol = '-';
    }
    else if (op == 3) {
        opSymbol = '*';
    }
    else if (op == 4) {
        opSymbol = '/';
    }

    cout << number << ". " << left << " " << opSymbol << " " << right << " = ";
}

int calculate(int left, int right, int op) {
    if (op == 1) {
        return left + right;
    }
    else if (op == 2) {
        return left - right;
    }
    else if (op == 3) {
        return left * right;
    }
    else if (op == 4) {
        return left / right;
    } else {
        return 0;
    }
}

int main() {

    /*
        Create a program that asks the user a simple math questions.
        The program should randomly decide the mathematical operation
        (addition, subtraction, multiplication, or division) with numbers
        no larger than 20. Division should be treated as integer division.

        Example:

        1. 11 / 11 = 1
        2. 5 / 3 = 1
        3. 9 / 6 = 0
        3. 9 / 6 = 1
        4. 4 * 3 = 12
        5. 10 + 2 = 12
        6. 4 * 10 = 40
        7. 3 - 12 = 

        Thank you for using our Math Quiz
    */


    RNG generator;
    int left, right, op;
    int count = 1;

    left = generator.get(1, 20);
    right = generator.get(1, 20);
    op = generator.get(1, 4);
    int answer = calculate(left, right, op);

    displayQuestion(count, left, right, op);

    int guess;
    while (cin >> guess) {
        if (guess == answer) {
            count++;
            left = generator.get(1, 20);
            right = generator.get(1, 20);
            op = generator.get(1, 4);
            answer = calculate(left, right, op);
        }
        displayQuestion(count, left, right, op);
    }

    cout << endl;
    cout << "Thank you for using our Math Quiz" << endl;

    return 0;
}