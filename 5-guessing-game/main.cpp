#include <iostream>
#include <ucm_random>
using namespace std;

int main() {

    /*
        Create a game that generates a random number and lets the user guess it.
        The user should get an unlimited number of attempts.
    */

    RNG generator;
    int x = generator.get(1, 10);
    bool guessedCorrectly = false;

    int guess;
    while (cin >> guess) {
        if (guess == x) {
            cout << "That is correct" << endl;
            guessedCorrectly = true;
            break;
        } else {
            cout << "That is incorrect" << endl;
        }
    }

    cout << endl;
    cout << "Thank you for playing." << endl;
    if (!guessedCorrectly) {
        cout << "The correct number was: " << x << endl;
    }

    return 0;
}