#include <iostream>
using namespace std;

int main() {

    /*
        Print the following sequence of numbers: 2, 4, 6, 8, 10
        using a while loop.
    */

    // for (int i = 2; i <= 10; i += 2) {
    //     cout << i;

    //     // print comma only when you are not printing the last number
    //     if (i < 10) {
    //         cout << ", ";
    //     }
    // }
    // cout << endl;


    int i = 2;

    // Example 1:
    // while (i <= 10) {
    //     cout << i;

    //     if (i < 10) {
    //         cout << ", ";
    //     }

    //     i += 2;
    // }
    // cout << endl;

    // Example 2:
    while (true) {
        cout << i;

        if (i < 10) {
            cout << ", ";
        }

        if (i == 10) {
            break;
        }
        
        i += 2;
    }
    cout << endl;

    return 0;
}