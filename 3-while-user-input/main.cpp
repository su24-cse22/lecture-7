#include <iostream>
using namespace std;

int main() {

    /*
        Create a program that computes the sum of all inputted numbers.
    */

    // Can use the file as input as well: `./app < input.txt`

    int x, total;

    total = 0;

    // while the user is giving us input (must type `ctrl + D` to stop)
    while (cin >> x) {
        // total = total + x
        total += x;
    }

    cout << "The sum is: " << total << endl;

    return 0;
}