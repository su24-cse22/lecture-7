#include <iostream>
using namespace std;

int main() {

    /*
        Create a program that checks if the user inputted the correct password.
        The user should get an unlimited number of attempts.
    */

    string password = "abc123";
    string guess;

    while (cin >> guess) {
        if (guess == password) {
            cout << "Access Granted" << endl;
            break;
        } else {
            cout << "Access Denied" << endl;
        }
    }

    return 0;
}